### 1. How to run the project
- clone the repository
- run `composer install`
- copy the .env.example file to .env and change the values accordingly (note `PROJECT_ROOT_FOLDER`, `APP_URL` and `DEMO_PAYMENT_API`)
- go to the `public` folder and start the built-in PHP server with `php -S localhost:3000`, or create a vhost file in your webserver config pointing to the project directory
- note that the project requires PHP >=7.4 and the DB driver of choice. An example SQLite database is provided here.

### 2. Architectural pattern of choice

The pattern I've chosen is Action/Domain/Responder, or ADR for short. A full description can be viewed
[here](https://github.com/pmjones/adr). 

The standard pattern used in nearly all of the current PHP web applications is MVC (actually, Model 2 MVC, since the original MVC is a GUI attern), with more or less variations. 
However, in practice this often leads to Controllers which are too big and handle requests for multiple areas of the application. Unless one takes special care to keep them small,
these tend to grow very quickly, and especially in legacy project, it is not uncommon to see Controllers with over 5k lines of code. This creates a lot
of problems for refactoring and testing. 

The alternative provided by the ADR pattern is to split every Controller request handling method in a separate Action and Responder class. 
The action takes care of collecting the input and passing it off to the domain layer, and then passing the result to the responder class, which is responsible for constructing
objects to pass on to the view. This separation of responsibilities is in line with the Single Responsibility Principle from the SOLID group, and helps a lot with testing and refactoring,
should the need arises. On top of that, having separate action/responder pair for every request makes it easier to track changes in VCS, as it is way more easier to see what requests have
changed over time, instead of having to dig through the history to find which controller method has been changed.

Of course, the ADR pattern has some drawbacks on its own 
(for example, it's disputed where session handling should happen - in the Domain layer or in a middleware before arriving at the entry point),
but this is outside the scope of this assignment.    

### 3. Possible performance optimizations

Not much could be done here, as the code is already quite lightweight with minimal amount of packages. But here are a few possible options:

- Use Redis to hold sessions - the default PHP sessions are file-based, which adds some additional disk I/O time
- Use Redis to hold intermediate state of registration data - considering that potentially some users would never reach the end of the
registration process, there's no point to clutter the actual database with half-finished registrations. However, the lifetime of the Redis session
should be considered, as in the case of restarting the server the intermediate data will be gone.
- Switch the templating engine package to one which supports template caching
- Add indexes to the columns in the DB tables which will potentially see a lot of reads

### 4. Things which could be done better

- Use a container for DI injection - all the `new()` operators couple tightly the classes one to another.
The first step of choice would be to inject them via a DI container instead. The `league/container` package
does a great job (not included due to some trouble detecting responder classes, which I could not resolve in reasonable amount of time)
- Abstract away ENV logic - similar to the issue above, having `$_ENV` all around the code makes it hard to change configuration should
the need arises. These belong in the container binding logic as well.
- Replace DB options with DSN - a small tweak, but makes it easier to switch databases (the Symfony framework uses the same approach)
- Implement reverse routing - by setting names for the routes and injecting the router in the current template. A second possible
step would be to replace the routing package entirely, as according to the authors naming routes slows resolving considerably  
- Add CSRF tokens to the step forms - basic security
- Add a PSR-3 logger - every modern application needs to know what's happened on every request  
- Implement global exception handler - to provide a sensible error page instead 
- Refactor DB model create() and update() methods - they could be extracted to methods with common logic, or a separate package for
handling DB logic could be added while still keeping resources footprint low.
- Service classes - services tend to grow quite quickly and combine a lot of responsibilities. Domain logic can be organized in
a more sensible package of layers. A good place to start would be implementing Repository/Gateway pattern, or something similar.
- Write a few tests - enough for testing the UI logic and the Domain layer