<?php

namespace Northbridg3\Wunder\Domain\Model;

use Northbridg3\Wunder\Domain\DB\DB;
use PDO;

trait CommonReadMethods
{
    public static function fromArray(array $values)
    {
        $obj = new static();

        foreach ($values as $key => $value) {
            if (property_exists(static::class, $key)) {
                $method = 'set' . static::snakeToCamel($key, true);
                $obj->$method($value);
            }
        }

        return $obj;
    }

    public static function hasAllProperties(array $keys)
    {
        foreach ($keys as $property) {
            if (property_exists(static::class, $property) === false) {
                return false;
            }
        }

        return true;
    }

    public function refresh($values)
    {
        if (static::hasAllProperties(array_keys($values)) === false) {
            throw new \LogicException('Cannot update a ' . static::$tableName . ' with an unknown property');
        }

        foreach ($values as $key => $value) {
            if (property_exists(static::class, $key)) {
                $method = 'set' . static::snakeToCamel($key, true);
                $this->$method($value);
            }
        }
    }

    public static function find(int $id)
    {
        $db = new DB();
        $pdo = $db->getPdo();
        $stmt = $pdo->prepare('SELECT * FROM ' . static::$tableName . ' WHERE id = :id');

        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();

        return $stmt->fetchObject(static::class);
    }

    public static function findBy(string $key, $value)
    {
        if (!property_exists(static::class, $key)) {
            throw new \UnexpectedValueException('Property does not exist');
        }

        $db = new DB();
        $pdo = $db->getPdo();
        $stmt = $pdo->prepare("SELECT * FROM " . static::$tableName . " WHERE $key = :value");

        $stmt->bindParam(':value', $value, PDO::PARAM_STR);
        $stmt->execute();

        return $stmt->fetchObject(static::class);
    }

    public static function findWhereIn(string $key, $values)
    {
        if (!property_exists(static::class, $key)) {
            throw new \UnexpectedValueException('Property does not exist');
        }

        $db = new DB();
        $pdo = $db->getPdo();
        $in  = str_repeat('?,', count($values) - 1) . '?';
        $stmt = $pdo->prepare("SELECT * FROM " . static::$tableName . " WHERE $key IN ($in)");

        $stmt->execute($values);

        return $stmt->fetchAll(PDO::FETCH_CLASS, static::class);
    }

    public static function get(int $from = null, int $limit = null)
    {
        $db = new DB();
        $pdo = $db->getPdo();
        $stmt = $pdo->prepare('SELECT * FROM ' . static::$tableName . ' WHERE id > :from LIMIT :limit');

        if (is_null($limit) or $limit <= 0) {
            $limit = $db->getDefaultResultLimit();
        }
        if (is_null($from) or $from <= 0) {
            $from = 0;
        }

        $stmt->bindParam(':from', $from, PDO::PARAM_INT);
        $stmt->bindParam(':limit', $limit, PDO::PARAM_INT);
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_CLASS, static::class);
    }

    public static function count()
    {
        $db = new DB();
        $pdo = $db->getPdo();
        $stmt = $pdo->query('SELECT COUNT(*) FROM ' . static::$tableName);

        return $stmt->fetchColumn();
    }

    public static function snakeToCamel($string, $capitalizeFirstCharacter = false)
    {
        $str = str_replace('_', '', ucwords($string, '_'));

        if (!$capitalizeFirstCharacter) {
            $str = lcfirst($str);
        }

        return $str;
    }
}
