<?php

namespace Northbridg3\Wunder\Domain\Model;

use Northbridg3\Wunder\Domain\DB\DB;
use PDO;

class UserAddress
{
    use CommonReadMethods;

    private static string $tableName = 'user_address';
    private int $id;
    private int $user_id;
    private string $street;
    private string $house_number;
    private string $zip;
    private string $city;
    private string $country;

    public function getId(): int
    {
        return $this->id;
    }

    public function getUserId(): int
    {
        return $this->user_id;
    }

    public function getStreet(): string
    {
        return $this->street;
    }

    public function getHouseNumber(): string
    {
        return $this->house_number;
    }

    public function getZip(): string
    {
        return $this->zip;
    }

    public function getCity(): string
    {
        return $this->city;
    }

    public function getCountry(): string
    {
        return $this->country;
    }

    public function setUserId(int $user_id): void
    {
        $this->user_id = $user_id;
    }

    public function setStreet(string $street): void
    {
        $this->street = $street;
    }

    public function setHouseNumber(string $house_number): void
    {
        $this->house_number = $house_number;
    }

    public function setZip(string $zip): void
    {
        $this->zip = $zip;
    }

    public function setCity(string $city): void
    {
        $this->city = $city;
    }

    public function setCountry(string $country): void
    {
        $this->country = $country;
    }

    public function create()
    {
        $db = new DB();
        $pdo = $db->getPdo();
        $stmt = $pdo->prepare(
            'INSERT INTO ' . static::$tableName .
            ' (user_id, street, house_number, zip, city, country) ' .
            'VALUES (:user_id, :street, :house_number, :zip, :city, :country)'
        );

        $stmt->bindValue(':user_id', $this->getUserId(), PDO::PARAM_INT);
        $stmt->bindValue(':street', $this->getStreet(), PDO::PARAM_STR);
        $stmt->bindValue(':house_number', $this->getHouseNumber(), PDO::PARAM_STR);
        $stmt->bindValue(':zip', $this->getZip(), PDO::PARAM_STR);
        $stmt->bindValue(':city', $this->getCity(), PDO::PARAM_STR);
        $stmt->bindValue(':country', $this->getCountry(), PDO::PARAM_STR);
        $stmt->execute();

        $result = $pdo->lastInsertId();
        $this->id = $result;
    }

    public function update($fields): int
    {
        if (empty($fields)) {
            throw new \LogicException('Cannot update a user address with empty properties');
        }
        if (empty($this->id)) {
            throw new \LogicException('Cannot update a user address with a missing ID');
        }
        if (static::hasAllProperties(array_keys($fields)) === false) {
            throw new \LogicException('Cannot update a user address with an unknown property');
        }

        $db = new DB();
        $pdo = $db->getPdo();

        $keys = array_keys($fields);
        $sqlColumnsArray = array_map(fn ($x) => $x . ' = :' . $x, $keys);
        $sqlColumns = implode(', ', $sqlColumnsArray);
        $stmt = $pdo->prepare("UPDATE " . static::$tableName . " SET $sqlColumns WHERE id = :id");

        $paramFields = array_combine(array_map(fn ($x) => ':' . $x, $keys), $fields);

        foreach ($paramFields as $param => $value) {
            $stmt->bindValue($param, $value, PDO::PARAM_STR);
        }
        $stmt->bindValue(':id', $this->getId(), PDO::PARAM_INT);
        $stmt->execute();

        $result = $stmt->rowCount();
        $this->refresh($fields);

        return $result;
    }
}
