<?php

namespace Northbridg3\Wunder\Domain\Model;

use Northbridg3\Wunder\Domain\DB\DB;
use PDO;

class User
{
    use CommonReadMethods;

    private static string $tableName = 'user';
    private int $id;
    private string $session_id;
    private string $first_name;
    private string $last_name;
    private string $telephone;

    public function getId(): int
    {
        return $this->id;
    }

    public function getSessionId(): string
    {
        return $this->session_id;
    }

    public function getFirstName(): string
    {
        return $this->first_name;
    }

    public function getLastName(): string
    {
        return $this->last_name;
    }

    public function getTelephone(): string
    {
        return $this->telephone;
    }

    public function setSessionId(string $session_id): void
    {
        $this->session_id = $session_id;
    }

    public function setFirstName(string $first_name): void
    {
        $this->first_name = $first_name;
    }

    public function setLastName(string $last_name): void
    {
        $this->last_name = $last_name;
    }

    public function setTelephone(string $telephone): void
    {
        $this->telephone = $telephone;
    }

    public function create()
    {
        $db = new DB();
        $pdo = $db->getPdo();
        $stmt = $pdo->prepare(
            'INSERT INTO ' . static::$tableName .
            ' (session_id, first_name, last_name, telephone) VALUES (:session_id, :first_name, :last_name, :telephone)'
        );

        $stmt->bindValue(':session_id', $this->getSessionId(), PDO::PARAM_STR);
        $stmt->bindValue(':first_name', $this->getFirstName(), PDO::PARAM_STR);
        $stmt->bindValue(':last_name', $this->getLastName(), PDO::PARAM_STR);
        $stmt->bindValue(':telephone', $this->getTelephone(), PDO::PARAM_STR);
        $stmt->execute();

        $result = $pdo->lastInsertId();
        $this->id = $result;
    }

    public function update($fields): int
    {
        if (empty($fields)) {
            throw new \LogicException('Cannot update a user with empty properties');
        }
        if (empty($this->id)) {
            throw new \LogicException('Cannot update a user with a missing ID');
        }
        if (static::hasAllProperties(array_keys($fields)) === false) {
            throw new \LogicException('Cannot update a user with an unknown property');
        }

        $db = new DB();
        $pdo = $db->getPdo();

        $keys = array_keys($fields);
        $sqlColumnsArray = array_map(fn ($x) => $x . ' = :' . $x, $keys);
        $sqlColumns = implode(', ', $sqlColumnsArray);
        $stmt = $pdo->prepare("UPDATE " . static::$tableName . " SET $sqlColumns WHERE id = :id");

        $paramFields = array_combine(array_map(fn ($x) => ':' . $x, $keys), $fields);

        foreach ($paramFields as $param => $value) {
            $stmt->bindValue($param, $value, PDO::PARAM_STR);
        }
        $stmt->bindValue(':id', $this->getId(), PDO::PARAM_INT);
        $stmt->execute();

        $result = $stmt->rowCount();
        $this->refresh($fields);

        return $result;
    }
}
