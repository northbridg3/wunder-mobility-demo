<?php

namespace Northbridg3\Wunder\Domain\Model;

use Northbridg3\Wunder\Domain\DB\DB;
use PDO;

class UserPaymentInformation
{
    use CommonReadMethods;

    private static string $tableName = 'user_payment_information';
    private int $id;
    private int $user_id;
    private string $account_owner;
    private string $iban;
    private ?string $payment_data_id = null;

    public function getId(): int
    {
        return $this->id;
    }

    public function getUserId(): int
    {
        return $this->user_id;
    }

    public function getAccountOwner(): string
    {
        return $this->account_owner;
    }

    public function getIban(): string
    {
        return $this->iban;
    }

    public function getPaymentDataId(): ?string
    {
        return $this->payment_data_id;
    }

    public function setUserId(int $user_id): void
    {
        $this->user_id = $user_id;
    }

    public function setAccountOwner(string $account_owner): void
    {
        $this->account_owner = $account_owner;
    }

    public function setIban(string $iban): void
    {
        $this->iban = $iban;
    }

    public function setPaymentDataId(?string $payment_data_id): void
    {
        $this->payment_data_id = $payment_data_id;
    }

    public function create()
    {
        $db = new DB();
        $pdo = $db->getPdo();
        $stmt = $pdo->prepare(
            'INSERT INTO ' . static::$tableName .
            ' (user_id, account_owner, iban) ' .
            'VALUES (:user_id, :account_owner, :iban)'
        );

        $stmt->bindValue(':user_id', $this->getUserId(), PDO::PARAM_INT);
        $stmt->bindValue(':account_owner', $this->getAccountOwner(), PDO::PARAM_STR);
        $stmt->bindValue(':iban', $this->getIban(), PDO::PARAM_STR);
        $stmt->execute();

        $result = $pdo->lastInsertId();
        $this->id = $result;
    }

    public function update($fields): int
    {
        if (empty($fields)) {
            throw new \LogicException('Cannot update a user payment data with empty properties');
        }
        if (empty($this->id)) {
            throw new \LogicException('Cannot update a user payment data with a missing ID');
        }
        if (static::hasAllProperties(array_keys($fields)) === false) {
            throw new \LogicException('Cannot update a user payment data with an unknown property');
        }

        $db = new DB();
        $pdo = $db->getPdo();

        $keys = array_keys($fields);
        $sqlColumnsArray = array_map(fn ($x) => $x . ' = :' . $x, $keys);
        $sqlColumns = implode(', ', $sqlColumnsArray);
        $stmt = $pdo->prepare("UPDATE " . static::$tableName . " SET $sqlColumns WHERE id = :id");

        $paramFields = array_combine(array_map(fn ($x) => ':' . $x, $keys), $fields);

        foreach ($paramFields as $param => $value) {
            $stmt->bindValue($param, $value, PDO::PARAM_STR);
        }
        $stmt->bindValue(':id', $this->getId(), PDO::PARAM_INT);
        $stmt->execute();

        $result = $stmt->rowCount();
        $this->refresh($fields);

        return $result;
    }
}
