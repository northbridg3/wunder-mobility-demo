<?php

namespace Northbridg3\Wunder\Domain\DB;

use Northbridg3\Wunder\Domain\DB\Strategy\DBStrategyContext;
use PDO;
use PDOException;

class DB
{
    private PDO $pdo;
    private int $defaultResultLimit = 10;

    public function __construct()
    {
        $this->defaultResultLimit = $_ENV['DEFAULT_RESULT_LIMIT'] ?? $this->defaultResultLimit;
        $dbStrategyContext = new DBStrategyContext();

        try {
            $this->pdo = $dbStrategyContext->constructPdo();
        } catch (PDOException $e) {
            throw new PDOException($e->getMessage(), (int)$e->getCode());
        }
    }

    public function getPdo()
    {
        return $this->pdo;
    }

    public function getDefaultResultLimit(): int
    {
        return $this->defaultResultLimit;
    }
}
