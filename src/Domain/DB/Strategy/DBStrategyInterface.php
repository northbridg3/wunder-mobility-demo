<?php

namespace Northbridg3\Wunder\Domain\DB\Strategy;

use PDO;

interface DBStrategyInterface
{
    public function constructPdo(): PDO;
}
