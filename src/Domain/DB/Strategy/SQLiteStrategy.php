<?php

namespace Northbridg3\Wunder\Domain\DB\Strategy;

use PDO;

class SQLiteStrategy
{
    public function constructPdo(): PDO
    {
        $location = $_ENV['PROJECT_ROOT_FOLDER'] . $_ENV['SQLITE_LOCATION'];

        $dsn = "sqlite:" . $location;

        $options = [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES => false,
        ];

        $pdo = new PDO($dsn, null, null, $options);

        if ($_ENV['SQLITE_FOREIGN_KEYS']) {
            $pdo->exec('PRAGMA foreign_keys = ON;');
        }

        return $pdo;
    }
}
