<?php

namespace Northbridg3\Wunder\Domain\DB\Strategy;

use PDO;

class MySQLStrategy implements DBStrategyInterface
{
    public function constructPdo(): PDO
    {
        $host = $_ENV['MYSQL_DB_HOST'];
        $port = $_ENV['MYSQL_DB_PORT'];
        $db = $_ENV['MYSQL_DB_NAME'];
        $user = $_ENV['MYSQL_DB_USER'];
        $password = $_ENV['MYSQL_DB_PASSWORD'];
        $charset = $_ENV['MYSQL_DB_CHARSET'];

        $dsn = "mysql:host=$host;port=$port;dbname=$db;charset=$charset";
        $options = [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES => false,
        ];

        return new PDO($dsn, $user, $password, $options);
    }
}
