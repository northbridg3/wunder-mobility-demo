<?php

namespace Northbridg3\Wunder\Domain\DB\Strategy;

use PDO;
use RuntimeException;

class DBStrategyContext
{
    public function constructPdo(): PDO
    {
        $dbType = $_ENV['DB_TYPE'];

        if ($dbType === 'MYSQL') {
            $strategy = new MySQLStrategy();
        } elseif ($dbType === 'SQLITE') {
            $strategy = new SQLiteStrategy();
        } else {
            throw new RuntimeException('This DB vendor is not supported');
        }

        return $strategy->constructPdo();
    }
}
