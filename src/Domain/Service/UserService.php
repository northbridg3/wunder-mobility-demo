<?php

namespace Northbridg3\Wunder\Domain\Service;

use Northbridg3\Wunder\Domain\Model\User;
use Northbridg3\Wunder\Domain\Model\UserAddress;
use Northbridg3\Wunder\Domain\Model\UserPaymentInformation;

class UserService
{
    public function createOrUpdate(array $data)
    {
        if (empty($data['session_id'])) {
            throw new \RuntimeException('A session ID is required');
        }

        $user = User::findBy('session_id', $data['session_id']);

        if ($user) {
            $user->update($data);
        } else {
            $user = User::fromArray($data);
            $user->create();
        }

        return $user;
    }

    public function findById(int $userId)
    {
        return User::find($userId);
    }
}
