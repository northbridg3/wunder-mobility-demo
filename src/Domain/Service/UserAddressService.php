<?php

namespace Northbridg3\Wunder\Domain\Service;

use Northbridg3\Wunder\Domain\Model\User;
use Northbridg3\Wunder\Domain\Model\UserAddress;
use Northbridg3\Wunder\Domain\Model\UserPaymentInformation;

class UserAddressService
{
    public function createOrUpdate(array $data)
    {
        if (empty($data['session_id'])) {
            throw new \RuntimeException('A session ID is required');
        }

        $user = User::findBy('session_id', $data['session_id']);

        if (empty($user)) {
            throw new \RuntimeException('An unknown user has been requested');
        }

        $address = UserAddress::findBy('user_id', $user->getId());
        unset($data['session_id']);

        if (empty($address)) {
            $address = UserAddress::fromArray($data);
            $address->setUserId($user->getId());
            $address->create();
        } else {
            $address->update($data);
        }

        return $address;
    }

    public function findByUserId(int $userId)
    {
        return UserAddress::findBy('user_id', $userId);
    }
}
