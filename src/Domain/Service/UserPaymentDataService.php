<?php

namespace Northbridg3\Wunder\Domain\Service;

use Northbridg3\Wunder\Domain\Model\User;
use Northbridg3\Wunder\Domain\Model\UserAddress;
use Northbridg3\Wunder\Domain\Model\UserPaymentInformation;

class UserPaymentDataService
{
    public function createOrUpdate(array $data)
    {
        if (empty($data['session_id'])) {
            throw new \RuntimeException('A session ID is required');
        }

        $user = User::findBy('session_id', $data['session_id']);

        if (empty($user)) {
            throw new \RuntimeException('An unknown user has been requested');
        }

        $paymentData = UserPaymentInformation::findBy('user_id', $user->getId());
        unset($data['session_id']);

        if (empty($paymentData)) {
            $paymentData = UserPaymentInformation::fromArray($data);
            $paymentData->setUserId($user->getId());
            $paymentData->create();
        } else {
            $paymentData->update($data);
        }

        return $paymentData;
    }

    public function findByUserId(int $userId)
    {
        return UserAddress::findBy('user_id', $userId);
    }
}
