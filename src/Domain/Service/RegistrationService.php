<?php

namespace Northbridg3\Wunder\Domain\Service;

use Northbridg3\Wunder\Domain\External\DemoPaymentService;

class RegistrationService
{
    private UserService $userService;
    private UserAddressService $userAddressService;
    private UserPaymentDataService $userPaymentDataService;
    private DemoPaymentService $demoPaymentService;

    public function __construct()
    {
        $this->userService = new UserService();
        $this->userAddressService = new UserAddressService();
        $this->userPaymentDataService = new UserPaymentDataService();
        $this->demoPaymentService = new DemoPaymentService();
    }

    public function handleUserData($userData)
    {
        $this->userService->createOrUpdate($userData);
    }

    public function handleUserAddressData($userAddressData)
    {
        $this->userAddressService->createOrUpdate($userAddressData);
    }

    public function handleUserPaymentData($userPaymentData)
    {
        $userPaymentData = $this->userPaymentDataService->createOrUpdate($userPaymentData);
        $userId = $userPaymentData->getUserId();

        $user = $this->userService->findById($userId);
        $userAddress = $this->userAddressService->findByUserId($userId);

        if (empty($user) or empty($userAddress)) {
            throw new \RuntimeException('Unexisting user or user address');
        }

        $paymentData = $this->demoPaymentService->postUserPaymentInformation($user, $userAddress, $userPaymentData);
        $userPaymentData->update(['payment_data_id' => $paymentData['paymentDataId']]);
    }
}