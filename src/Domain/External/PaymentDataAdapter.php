<?php

namespace Northbridg3\Wunder\Domain\External;

class PaymentDataAdapter
{
    private array $paymentData = [];

    public function __construct(array $paymentData = [])
    {
        $this->paymentData = $paymentData;
    }

    public function getPaymentDataId(): ?string
    {
        return $this->paymentData['payment_data_id'] ?? null;
    }
}