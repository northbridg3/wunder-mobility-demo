<?php

namespace Northbridg3\Wunder\Domain\External;

use Buzz\Browser;
use Buzz\Client\Curl;
use Northbridg3\Wunder\Domain\Model\User;
use Northbridg3\Wunder\Domain\Model\UserAddress;
use Northbridg3\Wunder\Domain\Model\UserPaymentInformation;
use Nyholm\Psr7\Factory\Psr17Factory;

class DemoPaymentService
{
    private Browser $browser;

    public function __construct()
    {
        $client = new Curl(new Psr17Factory());
        $this->browser = new Browser($client, new Psr17Factory());
    }

    public function postUserPaymentInformation(
        User $user,
        UserAddress $userAddress,
        UserPaymentInformation $userPaymentData
    ) {
        $data = [
            'customerId' => $user->getId(),
            'owner' => $userPaymentData->getAccountOwner(),
            'iban' => $userPaymentData->getIban()
        ];
        $uri = 'default/wunderfleet-recruiting-backend-dev-save-payment-data';

        $result = $this->browser->post(
            $_ENV['DEMO_PAYMENT_API'] . $uri,
            ['Content-Type' => 'application/json'],
            json_encode($data)
        );

        if ($result->getStatusCode() !== 200) {
            throw new \RuntimeException('Remote server could not complete the request');
        }

        $paymentDataResponse = $result->getBody()->getContents();
        return json_decode($paymentDataResponse, true);
    }
}