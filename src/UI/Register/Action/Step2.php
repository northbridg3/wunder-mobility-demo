<?php

namespace Northbridg3\Wunder\UI\Register\Action;

use Northbridg3\Wunder\UI\ActionInterface;
use Northbridg3\Wunder\UI\Register\Responder\Step2 as Responder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class Step2 implements ActionInterface
{
    private Responder $responder;

    public function __construct()
    {
        $this->responder = new Responder();
    }

    public function __invoke(ServerRequestInterface $request): ResponseInterface
    {
        return $this->responder->response();
    }
}
