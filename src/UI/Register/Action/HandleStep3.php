<?php

namespace Northbridg3\Wunder\UI\Register\Action;

use Northbridg3\Wunder\Domain\Service\RegistrationService;
use Northbridg3\Wunder\UI\ActionInterface;
use Northbridg3\Wunder\UI\Register\Responder\HandleStep3 as Responder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class HandleStep3 implements ActionInterface
{
    private Responder $responder;
    private RegistrationService $registrationService;

    public function __construct()
    {
        $this->responder = new Responder();
        $this->registrationService = new RegistrationService();
    }

    public function __invoke(ServerRequestInterface $request): ResponseInterface
    {
        $data = $request->getParsedBody();
        $data['session_id'] = session_id();
        $this->registrationService->handleUserPaymentData($data);
        return $this->responder->response();
    }
}
