<?php

namespace Northbridg3\Wunder\UI\Register\Responder;

use Northbridg3\Wunder\UI\BaseResponder;
use Psr\Http\Message\ResponseInterface;

class HandleStep2 extends BaseResponder
{
    public function response($data = null): ResponseInterface
    {
        $response = $this->responseFactory->createResponse(303);

        if (!in_array('/step-2', $_SESSION['completed_steps'])) {
            $_SESSION['completed_steps'][] = '/step-2';
        }

        return $response->withHeader('Location', '/step-3');
    }
}
