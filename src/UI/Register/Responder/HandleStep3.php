<?php

namespace Northbridg3\Wunder\UI\Register\Responder;

use Northbridg3\Wunder\UI\BaseResponder;
use Psr\Http\Message\ResponseInterface;

class HandleStep3 extends BaseResponder
{
    public function response($data = null): ResponseInterface
    {
        $response = $this->responseFactory->createResponse(303);

        if (!in_array('/step-3', $_SESSION['completed_steps'])) {
            $_SESSION['completed_steps'][] = '/step-3';
        }

        return $response->withHeader('Location', '/finish');
    }
}
