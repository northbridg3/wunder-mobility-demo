<?php

namespace Northbridg3\Wunder\UI\Register\Responder;

use Northbridg3\Wunder\UI\BaseResponder;
use Psr\Http\Message\ResponseInterface;

class Start extends BaseResponder
{
    public function response($data = null): ResponseInterface
    {
        $stepsToComplete = ['/', '/step-1', '/step-2', '/step-3', '/finish'];
        $currentStep = array_shift($stepsToComplete);

        if (in_array($currentStep, $_SESSION['completed_steps'] ?? [])) {
            $response = $this->responseFactory->createResponse(303);
            $remainingSteps = array_diff($stepsToComplete, $_SESSION['completed_steps']);
            $firstIncompleteStep = array_shift($remainingSteps);

            return $response->withHeader('Location', $firstIncompleteStep);
        }

        if (!in_array('/', $_SESSION['completed_steps'] ?? [])) {
            $_SESSION['completed_steps'][] = '/';
        }

        $response = $this->responseFactory->createResponse(200);
        $contents = $this->template->render('Register/start', ['title' => 'Start']);
        $response->getBody()->write($contents);

        return $response;
    }
}
