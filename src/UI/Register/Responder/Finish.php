<?php

namespace Northbridg3\Wunder\UI\Register\Responder;

use Northbridg3\Wunder\UI\BaseResponder;
use Psr\Http\Message\ResponseInterface;

class Finish extends BaseResponder
{
    public function response($data = null): ResponseInterface
    {
        $incompleteSteps = array_diff(['/', '/step-1', '/step-2', '/step-3'], $_SESSION['completed_steps'] ?? []);

        if (!empty($incompleteSteps)) {
            $response = $this->responseFactory->createResponse(303);
            $firstIncompleteStep = array_shift($incompleteSteps);
            return $response->withHeader('Location', $firstIncompleteStep);
        }

        $response = $this->responseFactory->createResponse(200);
        $contents = $this->template->render('Register/finish', ['title' => 'Success']);
        $response->getBody()->write($contents);

        return $response;
    }
}
