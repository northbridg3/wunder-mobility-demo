<?php

namespace Northbridg3\Wunder\UI\Register\Responder;

use Northbridg3\Wunder\UI\BaseResponder;
use Psr\Http\Message\ResponseInterface;

class HandleStep1 extends BaseResponder
{
    public function response($data = null): ResponseInterface
    {
        $response = $this->responseFactory->createResponse(303);

        if (!in_array('/step-1', $_SESSION['completed_steps'])) {
            $_SESSION['completed_steps'][] = '/step-1';
        }

        return $response->withHeader('Location', '/step-2');
    }
}
