<?php

namespace Northbridg3\Wunder\UI\Register\Responder;

use Northbridg3\Wunder\UI\BaseResponder;
use Psr\Http\Message\ResponseInterface;

class Step2 extends BaseResponder
{
    public function response($data = null): ResponseInterface
    {
        $incompleteSteps = array_diff(['/', '/step-1'], $_SESSION['completed_steps'] ?? []);

        if (!empty($incompleteSteps)) {
            $response = $this->responseFactory->createResponse(303);
            $firstIncompleteStep = array_shift($incompleteSteps);
            return $response->withHeader('Location', $firstIncompleteStep);
        }

        $stepsToComplete = ['/step-2', '/step-3', '/finish'];
        $currentStep = array_shift($stepsToComplete);

        if (in_array($currentStep, $_SESSION['completed_steps'])) {
            $response = $this->responseFactory->createResponse(303);
            $remainingSteps = array_diff($stepsToComplete, $_SESSION['completed_steps']);
            $firstIncompleteStep = array_shift($remainingSteps);
            return $response->withHeader('Location', $firstIncompleteStep);
        }

        $response = $this->responseFactory->createResponse(200);
        $contents = $this->template->render('Register/step2', ['title' => 'Step 2']);
        $response->getBody()->write($contents);

        return $response;
    }
}
