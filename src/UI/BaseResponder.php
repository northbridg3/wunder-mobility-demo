<?php

namespace Northbridg3\Wunder\UI;

use League\Plates\Engine;
use Nyholm\Psr7\Factory\Psr17Factory;
use Psr\Http\Message\ResponseFactoryInterface;

class BaseResponder
{
    protected Engine $template;
    protected ResponseFactoryInterface $responseFactory;

    public function __construct()
    {
        $this->template = new Engine($_ENV['PROJECT_ROOT_FOLDER'] . 'templates');
        $this->responseFactory = new Psr17Factory();
    }
}