<?php

namespace Northbridg3\Wunder;

use League\Route\Http\Exception\NotFoundException;
use League\Route\Router as LeagueRouter;
use Northbridg3\Wunder\UI\Middleware\SessionMiddleware;
use Northbridg3\Wunder\UI\Register\Action;
use Nyholm\Psr7\Factory\Psr17Factory;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class Router
{
    protected LeagueRouter $router;
    protected ResponseFactoryInterface $responseFactory;

    public function __construct()
    {
        $this->responseFactory = new Psr17Factory();
        $this->router = new LeagueRouter();
        $this->router->middleware((new SessionMiddleware())->options([
            'cookie_httponly' => true,
            'cookie_lifetime' => 365 * 24 * 60 * 60
        ]));
    }

    public function handleRequest(ServerRequestInterface $request): ResponseInterface
    {
        $this->registerStandardRoutes($request);

        try {
            return $this->router->dispatch($request);
        } catch (NotFoundException $e) {
            return $this->responseFactory->createResponse(404);
        }
    }

    private function registerStandardRoutes(ServerRequestInterface $request): void
    {
        $this->router->map('GET', '/', Action\Start::class);
        $this->router->map('GET', '/step-1', Action\Step1::class);
        $this->router->map('POST', '/step-1', Action\HandleStep1::class);
        $this->router->map('GET', '/step-2', Action\Step2::class);
        $this->router->map('POST', '/step-2', Action\HandleStep2::class);
        $this->router->map('GET', '/step-3', Action\Step3::class);
        $this->router->map('POST', '/step-3', Action\HandleStep3::class);
        $this->router->map('GET', '/finish', Action\Finish::class);
    }
}
