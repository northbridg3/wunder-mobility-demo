<?php $this->layout('template', ['title' => $this->e($title)]) ?>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-4">
        </div>
        <div class="col-md-4">
            <div class="main-content">
                <img class="mx-auto d-block img-fluid logo" alt="Bootstrap Image Preview" src="/assets/img/Glyph - Circle White.svg" />
                <h3 class="text-center">
                    Wunder Mobility demo application <?=$this->e($title)?>
                </h3>
                <p>Step 3 - payment information</p>
                <form method="post">
                    <div class="form-group row">
                        <label for="account_owner" class="col-4 col-form-label">Account owner</label>
                        <div class="col-8">
                            <input id="account_owner" name="account_owner" type="text" required="required" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="iban" class="col-4 col-form-label">IBAN</label>
                        <div class="col-8">
                            <input id="iban" name="iban" type="text" required="required" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="offset-4 col-8">
                            <button type="submit" class="btn btn-success float-right">Next</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
