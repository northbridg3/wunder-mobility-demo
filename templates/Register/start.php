<?php $this->layout('template', ['title' => $this->e($title)]) ?>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-4">
        </div>
        <div class="col-md-4">
            <div class="main-content">
                <img class="mx-auto d-block img-fluid logo" alt="Bootstrap Image Preview" src="/assets/img/Glyph - Circle White.svg" />
                <h3 class="text-center">
                    Wunder Mobility demo application
                </h3>
                <p>
                    On this page, you will complete a basic user registration. Click on start to begin.
                </p>
                <a href="step-1" class="btn btn-success float-right">
                    Start
                </a>
            </div>
        </div>
        <div class="col-md-4">
        </div>
    </div>
</div>
