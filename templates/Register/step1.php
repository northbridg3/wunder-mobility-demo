<?php $this->layout('template', ['title' => $this->e($title)]) ?>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-4">
        </div>
        <div class="col-md-4">
            <div class="main-content">
                <img class="mx-auto d-block img-fluid logo" alt="Bootstrap Image Preview" src="/assets/img/Glyph - Circle White.svg" />
                <h3 class="text-center">
                    Wunder Mobility demo application <?=$this->e($title)?>
                </h3>
                <p>Step 1 - personal information</p>
                <form method="post">
                    <div class="form-group row">
                        <label for="first_name" class="col-4 col-form-label">First name</label>
                        <div class="col-8">
                            <div class="input-group">
                                <input id="first_name" name="first_name" type="text" class="form-control" required="required">
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="last_name" class="col-4 col-form-label">Last name</label>
                        <div class="col-8">
                            <div class="input-group">
                                <input id="last_name" name="last_name" type="text" class="form-control" required="required">
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="telephone" class="col-4 col-form-label">Phone number</label>
                        <div class="col-8">
                            <div class="input-group">
                                <input id="telephone" name="telephone" type="tel" required="required" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="offset-4 col-8">
                            <button type="submit" class="btn btn-success float-right">Next</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
