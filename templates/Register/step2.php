<?php $this->layout('template', ['title' => $this->e($title)]) ?>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-4">
        </div>
        <div class="col-md-4">
            <div class="main-content">
                <img class="mx-auto d-block img-fluid logo" alt="Bootstrap Image Preview" src="/assets/img/Glyph - Circle White.svg" />
                <h3 class="text-center">
                    Wunder Mobility demo application <?=$this->e($title)?>
                </h3>
                <p>Step 2 - address information</p>
                <form method="post">
                    <div class="form-group row">
                        <label for="street" class="col-4 col-form-label">Street</label>
                        <div class="col-8">
                            <input id="street" name="street" type="text" class="form-control" required="required">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="house_number" class="col-4 col-form-label">House №</label>
                        <div class="col-8">
                            <input id="house_number" name="house_number" type="text" class="form-control" required="required">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="zip" class="col-4 col-form-label">ZIP/Postal code</label>
                        <div class="col-8">
                            <input id="zip" name="zip" type="text" class="form-control" required="required">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="city" class="col-4 col-form-label">City</label>
                        <div class="col-8">
                            <input id="city" name="city" type="text" class="form-control" required="required">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="country" class="col-4 col-form-label">Country</label>
                        <div class="col-8">
                            <input id="country" name="country" type="text" class="form-control" required="required">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="offset-4 col-8">
                            <button type="submit" class="btn btn-success float-right">Next</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
