<?php $this->layout('template', ['title' => $this->e($title)]) ?>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-4">
        </div>
        <div class="col-md-4">
            <div class="main-content">
                <img class="mx-auto d-block img-fluid logo" alt="Bootstrap Image Preview" src="/assets/img/Glyph - Circle White.svg" />
                <h3 class="text-center">
                    Wunder Mobility demo application <?=$this->e($title)?>
                </h3>
                <p class="text-center">You've completed the registration process</p>
            </div>
        </div>
        <div class="col-md-4">
        </div>
    </div>
</div>
