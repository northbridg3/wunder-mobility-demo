<?php

use Northbridg3\Wunder\Router;
use Narrowspark\HttpEmitter\SapiEmitter;
use Nyholm\Psr7\Factory\Psr17Factory;
use Nyholm\Psr7Server\ServerRequestCreator;

// Set flags for PSR-7 session compatibility
// See https://paul-m-jones.com/post/2016/04/12/psr-7-and-session-cookies
ini_set('session.use_trans_sid', false);
ini_set('session.use_cookies', false);
ini_set('session.use_only_cookies', true);
ini_set('session.cache_limiter', '');

require __DIR__ . '/../vendor/autoload.php';

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__ . '/../');
$dotenv->load();
$dotenv->required(['APP_URL', 'DB_TYPE']);

$router = new Router();
$psr17Factory = new Psr17Factory();

$creator = new ServerRequestCreator(
    $psr17Factory, // ServerRequestFactory
    $psr17Factory, // UriFactory
    $psr17Factory, // UploadedFileFactory
    $psr17Factory  // StreamFactory
);

$serverRequest = $creator->fromGlobals();
$response = $router->handleRequest($serverRequest);

$emitter = new SapiEmitter();
$emitter->emit($response);
